#!/usr/bin/env bash

sudo nvidia-smi -pl 144

sudo X :0 &
sleep 5
export DISPLAY=:0
IFS=$'\n'
for gpu in $(nvidia-smi -L); do
	id=$(echo ${gpu} | sed -e 's/GPU //g' -e 's/:.*//g')
	nvidia-settings -a [gpu:${id}]/GPUPowerMizerMode=1 -a [gpu:${id}]/GPUGraphicsClockOffset[3]=-200 -a [gpu:${id}]/GPUMemoryTransferRateOffset[3]=1200
done
sudo killall Xorg

sudo systemctl start ethminer
java -jar EthminerTGBot-1.0-SNAPSHOT.jar &