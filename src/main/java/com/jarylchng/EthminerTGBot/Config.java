package com.jarylchng.EthminerTGBot;

import lombok.Getter;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Config {
    private static volatile Config instance;

    private String botUsername;
    private String botToken;

    private String apiHost = "localhost";
    private int apiPort = 80;

    private List<Long> whitelist = new ArrayList<>();
    private long observer = 0;

    private String cmdReset;
    private String cmdReboot;

    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    @SneakyThrows
    private Config() {
        InputStream in = Config.class.getClassLoader().getResourceAsStream("config.json");
        JSONObject json = new JSONObject(convertStreamToString(in));
        for (Field field : Config.class.getDeclaredFields()) {
            try {
                if (field.getType() == String.class) {
                    String val = json.getString(field.getName());
                    field.set(this, val);
                } else if (field.getType() == int.class) {
                    int val = json.getInt(field.getName());
                    field.setInt(this, val);
                } else if (field.getType() == long.class) {
                    long val = json.getLong(field.getName());
                    field.setLong(this, val);
                } else if (field.getType() == List.class) {
                    JSONArray val = json.getJSONArray(field.getName());
                    for (int i = 0; i < val.length(); i++) {
                        ((List<Long>) field.get(this)).add(val.getLong(i));
                    }
                }
            } catch (JSONException e) {
                // ignore
            }
        }
    }

    private static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
