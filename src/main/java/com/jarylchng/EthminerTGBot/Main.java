package com.jarylchng.EthminerTGBot;

import lombok.SneakyThrows;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;

public class Main {
    @SneakyThrows
    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        botsApi.registerBot(Bot.getInstance());

        if (Config.getInstance().getObserver() != 0)
            new Thread(new Watchdog()).start();

        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                Bot.getInstance().sendMessage(Config.getInstance().getObserver(), "Watchdog: Deactivated.")
        ));
    }
}