package com.jarylchng.EthminerTGBot;

import com.jarylchng.EthminerTGBot.Ethminer.Ethminer;
import com.jarylchng.EthminerTGBot.Ethminer.EthminerConnection;
import com.jarylchng.EthminerTGBot.Ethminer.EthminerGPU;
import com.jarylchng.EthminerTGBot.Ethminer.EthminerStatus;
import lombok.SneakyThrows;

import java.time.Instant;
import java.util.ArrayDeque;

public class Watchdog implements Runnable {
    private static final int FIRST_RUN = 0;
    private static final int STABILISED = 1;
    private static final int FIRST_DOWN = 2;
    private static final int SECOND_DOWN = 3;
    private static final int THIRD_DOWN = 4;
    private static volatile int state = FIRST_RUN;

    private static volatile long lastShares = 0;
    private static volatile long lastRejects = 0;
    public static final ArrayDeque<Integer> histShares = new ArrayDeque<>(60);
    public static final ArrayDeque<Integer> histRejects = new ArrayDeque<>(60);

    private static volatile byte lastRejPercent = 0;

    private static volatile float lastRate = 0;

    private static volatile byte stagnant = 0;

    public static final ArrayDeque<Float> histHashrate = new ArrayDeque<>(60);
    public static final ArrayDeque<EthminerGPU[]> histGPU = new ArrayDeque<>(60);

    private static final Object lock = new Object();

    @Override
    public void run() {
        while (true) {
            synchronized (lock) {
                final EthminerStatus stats = Ethminer.status();

                // calculate current minute shares
                final long totalShares = (stats != null ? stats.getShares() : lastShares);
                final long totalRejects = (stats != null ? stats.getRejects() : lastRejects);
                final int curShares = (int) (totalShares - lastShares);
                final int curRejects = (int) (totalRejects - lastRejects);
                if (curShares < 0 || curRejects < 0) {
                    lastShares = totalShares;
                    lastRejects = totalRejects;
                } else {
                    lastShares += curShares;
                    lastRejects += curRejects;
                }

                // first run
                if (state == FIRST_RUN) {
                    state = STABILISED;
                    sendMessage("Activated.");
                    sleep();
                    continue;
                }

                // add to tracking history
                final EthminerGPU[] gpus = Ethminer.gpu();
                addToHistory(histShares, curShares);
                addToHistory(histRejects, curRejects);
                addToHistory(histHashrate, (stats != null ? stats.getHashrate() : 0));
                addToHistory(histGPU, gpus);

                // percentage rejects
                final long hourShares = histShares.stream().mapToLong(hist -> hist).sum();
                final long hourRejects = histRejects.stream().mapToLong(hist -> hist).sum();
                final byte perRejects = (byte) ((hourRejects * 1.0) / Math.max(1, (hourShares + hourRejects)) * 100);
                if (perRejects != lastRejPercent && perRejects % 2 == 0) {
                    sendMessage("Percentage of shares rejected for past hour changed: " + perRejects + "%");
                    lastRejPercent = perRejects;
                }

                // hashrate big decrease
                final float curRate = (stats != null ? stats.getHashrate() : 0);
                if (gpus != null) {
                    if (curRate < lastRate * (1.0 - ((1.0 / gpus.length) / 1.5))) {
                        sendMessage("Detected a big decrease in hashrate of " + ((lastRate - curRate) / 1000.0) + "MHs, likely a GPU became inactive.");
                    }
                    lastRate = curRate;
                }

                // stagnant share count
                if (curShares == 0) {
                    stagnant++;
                    if (stagnant >= 3)
                        sendMessage("Shares stagnated for the past " + stagnant + " minute(s)");
                } else {
                    stagnant = 0;
                }

                // miner down
                if (!Ethminer.ping() || stats == null || curRate == 0 || stagnant > 5) {
                    switch (state) {
                        case STABILISED:
                            sendMessage("Miner seems down! Attempting soft restart...");
                            Ethminer.restart();
                            state = FIRST_DOWN;
                            break;
                        case FIRST_DOWN:
                            sendMessage("Miner still down! Attempting change of connection...");
                            EthminerConnection[] connections = Ethminer.connections();
                            if (connections == null || connections.length == 1)
                                continue;
                            byte active = EthminerConnection.getActiveConnection(connections);
                            active++;
                            Ethminer.setConnection(active <= (connections.length - 1) ? active : 0);
                            state = SECOND_DOWN;
                            break;
                        case SECOND_DOWN:
                            sendMessage("Miner still down! Attempting hard reset...");
                            Ethminer.reset();
                            state = SECOND_DOWN;
                            break;
                        case THIRD_DOWN:
                            sendMessage("System probably froze! Attempting system reboot...");
                            Ethminer.reboot();
                            break;
                        default:
                            break;
                    }
                } else {
                    if (state > STABILISED) {
                        sendMessage("Miner is back up!");
                        state = STABILISED;
                    }
                }
            }
            sleep();
        }
    }

    private static void sendMessage(String msg) {
        Bot.getInstance().sendMessage(Config.getInstance().getObserver(), "Watchdog: " + msg);
    }

    private static <T> void addToHistory(ArrayDeque<T> hist, T curr) {
        if (curr != null) {
            if (hist.size() == 60)
                hist.poll();
            hist.add(curr);
        }
    }

    @SneakyThrows
    private static void sleep() {
        Thread.sleep(60000 - (Instant.now().toEpochMilli() % 60000)); // wait till the next perfect minute
    }
}
