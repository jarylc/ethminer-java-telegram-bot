package com.jarylchng.EthminerTGBot;

import com.jarylchng.EthminerTGBot.Ethminer.Ethminer;
import com.jarylchng.EthminerTGBot.Ethminer.EthminerConnection;
import com.jarylchng.EthminerTGBot.Ethminer.EthminerStatus;
import com.jarylchng.EthminerTGBot.utils.Chart;
import lombok.SneakyThrows;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.InputStream;
import java.time.Instant;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Bot extends TelegramLongPollingBot {
    private static final String SUCCESS = "Success";
    private static final String FAILED = "Failed";

    private static volatile Bot instance = null;

    static Bot getInstance() {
        return (instance == null ? new Bot() : instance);
    }

    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            long chatID = update.getMessage().getChatId();
            if (Config.getInstance().getWhitelist().contains(chatID)) {
                String msg = update.getMessage().getText().replace("@" + getBotUsername(), "");
                String[] args = msg.substring(1).split(" ");
                try {
                    switch (args[0]) {
                        case "ping":
                            sendMessage(chatID, Ethminer.ping() ? "Pong" : "No response");
                            break;
                        case "stat":
                            EthminerStatus stats = Ethminer.status();
                            sendMessage(chatID, (stats != null ? Ethminer.status().toString() : "No response"));
                            break;
                        case "gpu":
                            AtomicInteger totalPower = new AtomicInteger(0);
                            sendMessage(chatID, Arrays.stream(Ethminer.gpu())
                                    .map(gpu -> {
                                        totalPower.addAndGet(gpu.getPower());
                                        return gpu.toString() + "\n";
                                    })
                                    .collect(Collectors.joining()) +
                                    "\nTOTAL POWER USAGE: " + totalPower.get() + "W"
                            );
                            break;
                        case "shares_chart":
                            sendChart(chatID, Chart.shares());
                            break;
                        case "rate_chart":
                            sendChart(chatID, Chart.hashrate());
                            break;
                        case "gpu_chart":
                            sendChart(chatID, Chart.gpus());
                            break;
                        case "list":
                            EthminerConnection[] connections = Ethminer.connections();
                            sendMessage(chatID, IntStream.range(0, connections.length)
                                    .mapToObj(i -> i + ": " + connections[i] + "\n\n")
                                    .collect(Collectors.joining()));
                            break;
                        case "set":
                            if (args.length == 1)
                                sendMessage(chatID, "Error: specify a connection index ('/list' to list)");
                            else
                                sendMessage(chatID, (Ethminer.setConnection(Byte.parseByte(args[1])) ? SUCCESS : FAILED));
                            break;
                        case "rem":
                            if (args.length == 1)
                                sendMessage(chatID, "Error: specify a connection index ('/list' to list)");
                            else
                                sendMessage(chatID, (Ethminer.removeConnection(Byte.parseByte(args[1])) ? SUCCESS : FAILED));
                            break;
                        case "add":
                            if (args.length == 1)
                                sendMessage(chatID, "Error: specify a valid pool URI");
                            else if (!args[1].matches("(http[s]?|stratum[1-2]?\\+(tcp|ssl))://[A-z0-9]+\\.[A-z0-9]+@.+:[0-9]+"))
                                sendMessage(chatID, "Error: invalid URI");
                            else
                                sendMessage(chatID, (Ethminer.addConnection(args[1]) ? SUCCESS : FAILED));
                            break;
                        case "restart":
                            sendMessage(chatID, (Ethminer.restart() ? SUCCESS : FAILED));
                            break;
                        case "reset":
                            sendMessage(chatID, (Ethminer.reset() ? SUCCESS : FAILED));
                            break;
                        case "reboot":
                            sendMessage(chatID, (Ethminer.reboot() ? SUCCESS : FAILED));
                            break;
                        default:
                            break;
                    }
                } catch (IllegalArgumentException | NullPointerException e) {
                    sendMessage(chatID, FAILED + ": " + e.getMessage());
                }
            } else {
                sendMessage(chatID, chatID + " not whitelisted.");
            }
        }
    }

    @SneakyThrows
    private void sendChart(long chatID, InputStream chart) {
        execute(new SendPhoto()
                .setChatId(chatID)
                .setPhoto("" + Instant.now().getEpochSecond(), chart));
    }

    @SneakyThrows
    void sendMessage(long chatID, String text) {
        execute(new SendMessage()
                .setChatId(chatID)
                .setText(text));
    }

    public String getBotUsername() {
        return Config.getInstance().getBotUsername();
    }

    public String getBotToken() {
        return Config.getInstance().getBotToken();
    }
}
