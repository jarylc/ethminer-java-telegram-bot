package com.jarylchng.EthminerTGBot.Ethminer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.TimeUnit;

@AllArgsConstructor
@Getter
public class EthminerStatus {
    private String pool;
    private long runtime;
    private float hashrate;
    private long shares;
    private long rejects;

    public String toString() {
        return "Pool: " + pool + "\n" +
                "Runtime: " + convertToTimestamp(runtime) + "\n" +
                "Hashrate: " + hashrate / 1000.0 + " MH/s\n" +
                "Shares: " + shares + " submitted, " + rejects + " rejected";
    }

    private static String convertToTimestamp(long minutes) {
        short days = (short) TimeUnit.MINUTES.toDays(minutes);
        byte hours = (byte) (TimeUnit.MINUTES.toHours(minutes) - (days * 24));
        byte mins = (byte) (TimeUnit.MINUTES.toMinutes(minutes) - (TimeUnit.MINUTES.toHours(minutes) * 60));
        return (days > 0 ? days + "d " : "") + (hours > 0 ? hours + "h " : "") + mins + "m";
    }
}
