package com.jarylchng.EthminerTGBot.Ethminer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EthminerConnection {
    private final String connection;
    private final boolean active;

    public String toString() {
        return (active ? "[A] " : "") + connection;
    }

    public static byte getActiveConnection(EthminerConnection[] connections) {
        byte active = -1;
        for (byte i = 0; i < connections.length; i++) {
            if (connections[i].isActive()) {
                active = i;
                break;
            }
        }
        return active;
    }
}
