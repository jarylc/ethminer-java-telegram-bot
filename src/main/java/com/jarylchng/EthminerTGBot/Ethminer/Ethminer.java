package com.jarylchng.EthminerTGBot.Ethminer;

import com.jarylchng.EthminerTGBot.Config;
import lombok.Cleanup;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

public class Ethminer {
    private static final String RESULT = "result";

    public static boolean ping() {
        JSONObject json = sendMethod("ping");
        return json != null
                && json.has(RESULT)
                && json.getString(RESULT).equals("pong");
    }

    public static EthminerStatus status() {
        JSONObject json = sendMethod("getstat1");
        if (json == null)
            return null;

        JSONArray res = json.getJSONArray(RESULT);
        String pool = res.getString(7);
        long runtime = Long.parseLong(res.getString(1));
        String[] stats = res.getString(2).split(";");
        float hashrate = Float.parseFloat(stats[0]);
        long shares = Long.parseLong(stats[1]);
        long rejects = Long.parseLong(stats[2]);

        return new EthminerStatus(pool, runtime, hashrate, shares, rejects);
    }

    public static EthminerGPU[] gpu() {
        JSONObject json = sendMethod("getstatdetail");
        if (json == null)
            return null;

        JSONArray res = json.getJSONObject(RESULT).getJSONArray("devices");
        EthminerGPU[] gpus = new EthminerGPU[res.length()];
        for (int i = 0; i < res.length(); i++) {
            JSONObject hardware = res.getJSONObject(i).getJSONObject("hardware");
            JSONArray sensors = hardware.getJSONArray("sensors");

            String pci = hardware.getString("pci");
            String model = hardware.getString("name");
            byte temp = (byte) sensors.getInt(0);
            byte fan = (byte) sensors.getInt(1);
            short power = (short) sensors.getInt(2);

            gpus[i] = new EthminerGPU(pci, model, temp, fan, power);
        }

        return gpus;
    }

    public static EthminerConnection[] connections() {
        JSONObject json = sendMethod("getconnections");
        if (json == null)
            return null;
        JSONArray res = json.getJSONArray(RESULT);

        EthminerConnection[] connections = new EthminerConnection[res.length()];
        for (int i = 0; i < res.length(); i++) {
            JSONObject conn = res.getJSONObject(i);
            connections[i] = new EthminerConnection(conn.getString("uri"), conn.getBoolean("active"));
        }

        return connections;
    }

    public static boolean setConnection(byte i) {
        JSONObject json = sendMethod("setactiveconnection", "index", "" + i);
        if (json == null)
            return false;
        return json.has(RESULT) && json.getBoolean(RESULT);
    }

    public static boolean removeConnection(byte i) {
        JSONObject json = sendMethod("removeconnection", "index", "" + i);
        if (json == null)
            return false;
        return json.has(RESULT) && json.getBoolean(RESULT);
    }

    public static boolean addConnection(String uri) {
        JSONObject json = sendMethod("addconnection", "uri", uri);
        if (json == null)
            return false;
        return json.has(RESULT) && json.getBoolean(RESULT);
    }

    public static boolean restart() {
        JSONObject json = sendMethod("restart");
        if (json == null)
            return false;
        return json.has(RESULT) && json.getBoolean(RESULT);
    }

    public static boolean reset() {
        int status = -1;
        try {
            Process process = Runtime.getRuntime()
                    .exec(Config.getInstance().getCmdReset());
            status = process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return status == 0;
    }

    public static boolean reboot() {
        try {
            Process process = Runtime.getRuntime()
                    .exec(Config.getInstance().getCmdReboot());
            return process.waitFor(30, TimeUnit.SECONDS);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static JSONObject sendMethod(String method, String... args) {
        try (Socket socket = new Socket(Config.getInstance().getApiHost(), Config.getInstance().getApiPort())) {
            socket.setSoTimeout(5000);

            @Cleanup DataInputStream is = new DataInputStream(socket.getInputStream());
            @Cleanup DataOutputStream os = new DataOutputStream(socket.getOutputStream());

            JSONObject payload = new JSONObject();
            payload.put("id", "0");
            payload.put("jsonrpc", "2.0");
            payload.put("method", "miner_" + method);
            if (args.length > 0) {
                JSONObject params = new JSONObject();
                for (int i = 0; i < args.length; i += 2) {
                    int num = -1;
                    try {
                        num = Integer.parseInt(args[i + 1]);
                    } catch (NumberFormatException e) {
                        // ignore
                    }
                    params.put(args[i], (num != -1 ? num : args[i + 1]));
                }


                payload.put("params", params);
            }

            @Cleanup PrintWriter pw = new PrintWriter(os);
            pw.println(payload.toString());
            pw.flush();

            return new JSONObject(new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)).readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Ethminer() {
    }
}
