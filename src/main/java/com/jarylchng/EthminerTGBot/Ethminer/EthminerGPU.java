package com.jarylchng.EthminerTGBot.Ethminer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EthminerGPU {
    private final String pci;
    private final String model;
    private final byte temp;
    private final byte fan;
    private final short power;

    @Override
    public String toString() {
        return pci + ": " + model + "\n" +
                "   Temp: " + temp + "°C\n" +
                "   Fan: " + fan + "%\n" +
                "   Power: " + power + "W";
    }
}
