package com.jarylchng.EthminerTGBot.utils;

import com.jarylchng.EthminerTGBot.Ethminer.EthminerGPU;
import com.jarylchng.EthminerTGBot.Watchdog;
import lombok.SneakyThrows;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.Styler;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayDeque;

public class Chart {
    private static final String MINUTE = "Minute";
    private static final String PER_MINUTE_FOR_THE_PAST_HOUR = " per minute for the past hour";

    @SneakyThrows
    public static InputStream shares() {
        ArrayDeque<Integer> accepted = Watchdog.histShares.clone();
        ArrayDeque<Integer> rejected = Watchdog.histRejects.clone();

        XYChart chart = createChart("Shares");

        double total = 0;
        int[] acceptedX = new int[accepted.size()];
        int[] acceptedY = new int[accepted.size()];
        float[] avgAcceptedX = new float[accepted.size()];
        float[] avgAcceptedY = new float[accepted.size()];
        for (byte i = 1; !accepted.isEmpty(); i++) {
            int count = accepted.poll();
            acceptedX[i-1] = i;
            acceptedY[i-1] = count;

            total += count;
            avgAcceptedX[i-1] = i;
            avgAcceptedY[i-1] = (float) (total / i);
        }
        chart.addSeries("Accepted", acceptedX, acceptedY);
        chart.addSeries("Accepted (avg)", avgAcceptedX, avgAcceptedY);

        int[] rejectedX = new int[rejected.size()];
        int[] rejectedY = new int[rejected.size()];
        for (byte i = 1; !rejected.isEmpty(); i++) {
            rejectedX[i-1] = i;
            rejectedY[i-1] = rejected.poll();
        }
        chart.addSeries("Rejected", rejectedX, rejectedY);

        return chartToInputStream(chart);
    }

    @SneakyThrows
    public static InputStream hashrate() {
        ArrayDeque<Float> hashrates = Watchdog.histHashrate.clone();

        XYChart chart = createChart("Hashrate (MH/s)");

        double total = 0;
        float[] x = new float[hashrates.size()];
        float[] y = new float[hashrates.size()];
        float[] avgX = new float[hashrates.size()];
        float[] avgY = new float[hashrates.size()];
        for (byte i = 1; !hashrates.isEmpty(); i++) {
            float mhs = hashrates.poll() / 1000;
            x[i-1] = i;
            y[i-1] = mhs;

            avgX[i-1] = i;
            total += mhs;
            avgY[i-1] = (float) (total / i);
        }
        chart.addSeries("Current", x, y);
        chart.addSeries("Average", avgX, avgY);

        return chartToInputStream(chart);
    }

    @SneakyThrows
    public static InputStream gpus() {
        ArrayDeque<EthminerGPU[]> allGPUs = Watchdog.histGPU.clone();

        XYChart chart = createChart("Temperature (°C)");

        int count = allGPUs.peek().length;
        String[] series = new String[count];
        int[][] x = new int[count][allGPUs.size()];
        int[][] y = new int[count][allGPUs.size()];
        for (int min = 1; !allGPUs.isEmpty(); min++) {
            EthminerGPU[] gpus = allGPUs.poll();
            for (int i = 0; i < gpus.length; i++) {
                EthminerGPU gpu = gpus[i];
                if (allGPUs.isEmpty())
                    series[i] = gpu.getPci() + " " + gpu.getModel();
                x[i][min-1] = min;
                y[i][min-1] = gpu.getTemp();
            }
        }

        for (int i = 0; i < series.length; i++) {
            chart.addSeries(series[i], x[i], y[i]);
        }

        return chartToInputStream(chart);
    }

    private static XYChart createChart(String name) {
        XYChart chart = new XYChartBuilder().width(800).height(600)
                .title(name + PER_MINUTE_FOR_THE_PAST_HOUR)
                .xAxisTitle(MINUTE)
                .yAxisTitle(name).build();
        chart.getStyler().setXAxisMin(1.0);
        chart.getStyler().setLegendPosition(Styler.LegendPosition.OutsideS);
        return chart;
    }

    @SneakyThrows
    private static InputStream chartToInputStream(XYChart chart) {
        BufferedImage image = BitmapEncoder.getBufferedImage(chart);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "gif", os);
        return new ByteArrayInputStream(os.toByteArray());
    }

    private Chart() {
    }
}
