## Introduction
A simple Telegram bot to manage [Ethminer](https://github.com/ethereum-mining/ethminer), requires self compilation for now to use.

## Features
* Commands (can be copied to BotFather)
    ```
    ping - Ping miner
    stat - Mining stats
    gpu - GPU information
    shares_chart - Shares chart
    rate_chart - Hashrate chart
    gpu_chart - GPU temperature chart
    list - List of connections
    set - (index), Set active connection
    rem - (index), Remove connection
    add - (uri), Add URI to connection list
    restart - Soft restart miner
    reset - Hard restart miner
    reboot - Reboot system
    ```
* Watchdog (every minute)
    * Automatically detects when Ethminer is down and proceeds to soft restart -> change connection -> hard restart -> reboot system
    * Inform of 2% changes to the percentage of rejected shares sent
    * Inform if shares count has not changed for 3 minutes and above
    * Detecting of notable hashrate decrease

## Forking & usage instructions
Edit [config.json](https://gitlab.com/jarylc/ethminer-java-telegram-bot/blob/master/src/main/resources/config.json) accordingly
```json5
{
  "botUsername": "MyBot", // username of bot
  "botToken": "011899988:MyBoTsuPersEcreT", // api key of bot
  "apiHost": "localhost", // host of Ethminer API
  "apiPort": 80, // port of Ethminer API
  "whitelist": [
    0
  ], // list of whitelisted chat IDs
  "observer": 0, // main chat ID for watchdog output
  "cmdReset": "sudo systemctl restart ethminer", // command to hard reset Ethminer
  "cmdReboot": "sudo reboot" // command to reboot system
}
```
Afterwards just compile and run using Java.
You may want to set config.json to skip worktree to prevent uploading to git.
```console
$ git update-index --skip-worktree src/main/resources/config.json
```